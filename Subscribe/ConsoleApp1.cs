using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
namespace ConsoleApp1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Subscribe();
        }

         static public void Subscribe()
        {
            ConnectionFactory connectionFactory = new ConnectionFactory();
              connectionFactory.Uri = "amqp://54.172.146.107:10033";
            using (IConnection connection = connectionFactory.CreateConnection()) {
                using(IModel channel = connection.CreateModel()) {
                    channel.QueueDeclare("sst-docker-queue", false, false, false, null);
                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine(" [x] Received {0}", message);
                    };
                    channel.BasicConsume(queue: "sst-docker-queue",
                        noAck: true,
                        consumer: consumer);
                    Console.WriteLine("Press Enter to Quit. " + DateTime.Now);
                    Console.ReadLine();
                }
            }
        }
    }
}
