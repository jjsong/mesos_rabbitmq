using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main(string[] args)
        {
             Publish();
        }

        static public void Publish()
        {
            Random rnd = new Random();
            ConnectionFactory factory = new ConnectionFactory();

            // The next six lines are optional:
            factory.UserName = ConnectionFactory.DefaultUser;
            factory.Password = ConnectionFactory.DefaultPass;
            factory.VirtualHost = ConnectionFactory.DefaultVHost;
            factory.HostName = "54.172.146.107";
            factory.Port     = 10033;

            // You also could do this instead:
            factory.Uri = "amqp://54.172.146.107:10033";

            IConnection connection = factory.CreateConnection();

            IModel channel = connection.CreateModel();

            channel.QueueDeclare("sst-docker-queue", // queue
                false, // durable
                false, // exclusive
                false, // autoDelete
                null); // arguments

            while(true) {
              int val = rnd.Next(200, 500);
              string str = "Hello, World! " + val + " " + DateTime.Now;
              byte[] message = Encoding.UTF8.GetBytes(str);
                channel.BasicPublish(string.Empty, // exchange
                    "sst-docker-queue", // routingKey
                    null, // basicProperties
                    message); // body

              Console.WriteLine("sent: " + str );
              Thread.Sleep(val);
            }
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            channel.Close();
            connection.Close();
        }
    }
}
